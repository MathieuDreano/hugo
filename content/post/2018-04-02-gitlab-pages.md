---
title: Gitlab Pages & Static Site Generator
subtitle: Créer, héberger et maintenir simplement ce blog
date: 2018-02-04
tags: ["gitlab", "git", "site", "ssg", "hugo"]
---

**Gitlab Pages** permet d'héberger un site directement sur Gitlab. C'est justement de cette manière qu'est geré ce blog.

![Gitlab CI menu run ](/images/gitlab-pages/gitlab-pages-logo.png)

Vous pourrez ensuite l'administrer grâce à générateur de site statique et le versionner comme n'importe quel repository git.</br>
A noter que service est entièrement gratuit...

<!--more-->

Pour ce faire il vous faudra forker un projet existant parmi les dizaines de [static site generator](https://gitlab.com/pages) disponibles.
Dans cet exemple on choisira **hugo**.

Une fois que vous avez votre propre repository il vous suffira de déclencher un job via le panel CI/CD pour publier votre site.
![Gitlab CI menu run](/images/gitlab-pages/ci-menu-run.png)

Vous pouvez vérifier, votre site est déjà disponible: https://username.gitlab.io/hugo/ (remplacer username).

La documentation complète de gitlab pages est disponible ici : [gitlab pages documentation](https://docs.gitlab.com/ee/user/project/pages/index.html)

## Static Site Generator (SSG)

Maintenant que le site est en ligne nous allons pouvoir l'administrer grâce à un 'générateur de site statique'. Kezako ?

Dans l'étape précédente je vous avais demandé de choisir 'hugo' comme base de votre site. Hugo EST un générateur de site.

Il en existe de nombreux avec chacun ses avantages et inconvénients (voir [ici](https://www.staticgen.com/)) mais le principe de base reste le même.

Il s'agit d'un 'outil' qui générera pour vous des pages statiques (html, css, js) à partir de documents que vous rédigerez dans des formats très simples (ex: MarkDown) .

A chaque modification le générateur va reconstruire intégralement le site et créer des pages "prêtes à l'emploi".
Ces pages sont ensuite servies aux visiteurs sans nécessiter aucun traitement côté serveur.

Ainsi vous pourrez vous concentrer sur le contenu et laisser de générateur faire son travail.

Pour exemple, voici comment est rédigée la page que vous êtes en train de lire:
![Gitlab CI menu run](/images/gitlab-pages/markdown.png)

Ce type de procédé convient parfaitement pour la rédaction de documentations ou bien comme ici pour un blog.

Les principaux avantages sont les suivants:

**Rapidité**:</br>
Aucun traitement côté serveur excepté le fait de retourner la page.

**Sécurité**:</br>
Les pages sont statiques, il y a donc moins de possibilité pour un hacker de manipuler votre site. Pas de plugins avec des failles de sécurité, pas d'accès à une base de données.

**Version control**:</br>
Contrairement à un CMS style WordPress il est facile de garder une trace de toutes les modifications effectuées et nous avons également la possibilité de travailler par branches.

Un SSG vous offre un cadre prédéfini mais il vous est bien entendu possible de configurer ce générateur. C'est que nous allons voir par la suite.

### Hugo

Pour gérer ce site mon choix s'est arrêté sur [Hugo](https://gohugo.io/).
![hugo logo](/images/gitlab-pages/hugo-logo.png)

Plusieurs raisons à cela, c'est un générateur populaire et la documentation est excellente, des thème intéressants (*beautifulhugo* ici) et la possibilité de le lancer localement très simplement.

L'arborescence du repertoire de travail reste relativement simple.

**/post** contient les fichiers markdown correspondant à chaque article.</br>
**/theme** contient tous les fichiers permettant au blog d'avoir une apparence convenable.</br>
**config.toml** permet de paramétrer le générateur Hugo

![hugo logo](/images/gitlab-pages/hugo-structure.png)

Plus vous découvrirez Hugo plus ce répertoire s'étoffera mais pour le moment c'est tout ce dont vous avez besoin.

#### Developpement

Afin de tester vos modifications vous avez plusieurs options:

1. Générer localement votre site, il se trouvera dans le répertoire *public* (ou publishDir de votre config.toml).
```
hugo
```

1. Lancer localement un serveur, accessible via localhost:1313. A chaque modification d'un fichier votre site est mis à jour.
```
hugo server
```

#### Customize with theme

De nombreux thèmes sont disponibles pour Hugo. ([themes on github](https://github.com/gohugoio/hugoThemes))

Il vous faut cloner le thème de votre choix dans le répertoire **themes** et ensuite modifier votre fichier de config.

```
cd themes
git clone https://github.com/laozhu/my-chosen-hugo-theme.git
```

Puis config.toml
```
theme = "beautifulhugo"
```

A vous de jouer... Enjoy!
